#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 50
struct BOOK
{
	char title[N];
	char author[N];
	int year;
};
void get_book(struct BOOK *arr, int temp)
{
	FILE *file;
	int i;
	char str[N];
	file = fopen("book.txt", "rt");
	for(i = 0; i < temp; i++)
        {
        fgets(arr[i].title, N, file);
		arr[i].title[strlen(arr[i].title)-1] = '\0';
        fgets(arr[i].author, N, file);
		arr[i].author[strlen(arr[i].author)-1] = '\0';
        fgets(str, N, file);
        arr[i].year = atoi(str);
		fgets(str, N, file);
        }
	fclose(file);
}
int get_year(const void* a, const void* b)
{
	return ((struct BOOK*)a)->year - ((struct BOOK*)b)->year;
}
void print_book(struct BOOK *arr, int temp)
{
	int i;
	for(i = 0; i < temp; i++)
        {
        	printf("%d: %s,  %s,  %d\n", i+1, arr[i].title, arr[i].author, arr[i].year);
        }
}
int compare_author (const void* a, const void* b)
{
	return strcmp(((struct BOOK*)a)->author, ((struct BOOK*)b)->author);
}
int count()
{
	int temp = 0;
	int i = 1;
	FILE *file;
	char str[N];
	file = fopen("book.txt", "r");
	while(fgets(str, N, file))
	{
		if((temp + 1) / 4 != i)
		{
			temp++;
		} else
			i++;
	}
	fclose(file);
	return temp/3;
}
int main()
{
	struct BOOK *_book;
	int temp;
	temp = count();
	_book = (struct BOOK *) malloc(temp * sizeof(struct BOOK));
	get_book(_book, temp);
	printf("\nCatalog books:\n");
	print_book(_book, temp);
	qsort(_book, temp, sizeof(struct BOOK), get_year);
	printf("\nMin year:\n");
	printf("%s,  %s,  %d\n", _book[0].title, _book[0].author, _book[0].year);
	printf("\nMax year:\n");
	printf("%s,  %s,  %d\n", _book[temp-1].title, _book[temp-1].author, _book[temp-1].year);
	qsort(_book, temp, sizeof(struct BOOK), compare_author);
    printf("\nSorted catalog:\n");
    print_book(_book, temp);
	free(_book);
	return 0;
}
